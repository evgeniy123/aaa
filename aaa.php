 <?php


namespace App\Controller\Api\Project;

use App\Model\Gitlab\Entity\GitlabRepository;
use App\Model\Project\Entity\Project\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Project\UseCase\GetProject as GetProject;
use App\Validator\Validator;


/**
 * Class ProjectController
 * @package App\Controller\Api\Project
 */
class ProjectController extends AbstractController
{
    
    
    /**
     * @Route("/project", name="project.get", methods={"POST"})
     * @param Request $request
     * @param GetProject\Handler $handler
     * @return Response
     * @throws \Exception
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function getProject(Request $request, GetProject\Handler $handler): Response
    {

        $this->validator->validate($command);
        $out = $handler->handle($command);

        return new JsonResponse(
            [
                'commit' => $out
            ]);
    }

}
